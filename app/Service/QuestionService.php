<?php


namespace App\Service;


use App\Question;
use App\QuestionOption;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Question\ChoiceQuestion;

class QuestionService
{
    public function get()
    {
        return DB::table('questions')
            ->select('surveys.title as title','questions.question as question','questions.category as category','questions.id as id')
            ->join('surveys','surveys.id','=','questions.survey_id')
            ->get();
    }
    public function question($id)
    {
        return Question::with('options')->where('id',$id)->first();
    }
    public function create($data)
    {
        $question   =   new Question();
        $question->question =   $data['question'];
        $question->survey_id =   $data['survey'];
        $question->category  =  $data['category'];
        $question->save();

        return true;

    }

    /**
     * delete question
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $question   =  Question::where('id',$id)->delete();

        return $question?true:false;
    }
    public function update($data,$id)
    {
        $question   =   Question::where('id',$id)->first();
        $question->question =   $data['question'];
        $question->survey_id =   $data['survey'];
        $question->category =   $data['category'];
        $question->save();

        return true;


    }

}
