<?php


namespace App\Service;


use App\Survey;

class SurveyService
{
    /**
     * get all survey
     * @return Survey[]|\Illuminate\Database\Eloquent\Collection
     */
    public function get()
    {
        return Survey::all();
    }
    public function survey($id)
    {
        return Survey::where('id',$id)->first();
    }

    /**
     * create new survey
     * @param $data
     * @return Survey|false
     */
    public function create($data)
    {
        $survey =   new Survey();
        $survey->title  =   $data['title'];
        $survey->description  =   $data['description'];
        $survey->fees  =   $data['fees'];
        $survey->slug   =   $this->unique_slug($data['title']);
        return $survey->save()?$survey:false;
    }

    /**
     * generate unique slug for survey
     * @param string $title
     * @param string $model
     * @return string|string[]|null
     */

    public function unique_slug($title = '', $model = 'Survey'){
        $slug = str_slug($title);
        if ($slug === ''){
            $string = mb_strtolower($title, "UTF-8");;
            $string = preg_replace("/[\/\.]/", " ", $string);
            $string = preg_replace("/[\s-]+/", " ", $string);
            $slug = preg_replace("/[\s_]/", '-', $string);
        }

        //get unique slug...
        $nSlug = $slug;
        $i = 0;

        $model = str_replace(' ','',"\App\ ".$model);
        while( ($model::where('slug', $nSlug)->count()) > 0){
            $i++;
            $nSlug = $slug.'-'.$i;
        }
        if($i > 0) {
            $newSlug = substr($nSlug, 0, strlen($slug)) . '-' . $i;
        } else
        {
            $newSlug = $slug;
        }
        return $newSlug;
    }

    /**
     * delete survey
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $survey =   Survey::where('id',$id)->delete();
        return $survey?true:false;
    }

    public function update($data,$id)
    {
        $survey =   Survey::where('id',$id)->first();
        $survey->title=$data['title'];
        $survey->description  =   $data['description'];
        $survey->fees  =   $data['fees'];
        $survey->slug   = $this->unique_slug($data['title']);
        return $survey->save()?true:false;
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function getBySlug($slug)
    {
       return $survey =   Survey::where('slug',$slug)->first();



    }


}
