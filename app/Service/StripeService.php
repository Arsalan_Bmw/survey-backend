<?php

namespace App\Service;

use App\Comment;
use App\Forum;
use App\Order;
use App\OrderItem;
use App\SurveyAttempt;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotificationMail;
use Mpociot\VatCalculator\VatCalculator;
use Stripe\Checkout\Session;
use Stripe\Customer;
use Stripe\Invoice;
use Stripe\SetupIntent;
use Stripe\Subscription;

class StripeService
{

    public function __construct()
    {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
    }

    /**
     * Method to retrieve the session for a given session id
     * @param $session_id the session id from stripe
     * @return Session|null
     */
    public function getSession($session_id)
    {
        try {
            $session = \Stripe\Checkout\Session::retrieve($session_id);
        } catch (\Exception $e) {
            $session = null;
        }
        return $session;
    }

    public function getSetupIntent($setup_intent_id)
    {
        try {
            $si = SetupIntent::retrieve(
                $setup_intent_id
            );
        } catch (\Exception $e) {
            $si = null;
        }

        return $si;
    }

    public function createNewCheckoutSession()
    {
        $surveyAttempt = new SurveyAttempt();
        $surveyAttempt->code=mt_rand();
        $surveyAttempt->survey_id=1;
        $surveyAttempt->save();
        $hash = encrypt($surveyAttempt->id);

        $session = \Stripe\Checkout\Session::create([
            'success_url' => route('success', ['hash' => $hash]),
            'cancel_url' => route('error', ['hash' => $hash]),
            'payment_method_types' => ['card'],
            'line_items' => [
                [
                    'price_data' => [
                        'currency' => 'eur',
                        'unit_amount' => 499,
                        'product_data' => [
                            'name' => 'test',
                        ],
                    ],
                    'quantity' => 1,
                ],
            ],
            'mode' => 'payment',
        ]);
        return $session;
    }

    /**
     * Retrieves the stripe event.
     * Needed for webhoooks.
     *
     * @return \Stripe\Event|null
     */
    public function getStripeEvent()
    {
        $endpoint_secret = env('STRIPE_WEBHOOK_KEY_CUSTOMER_CHECKOUT');
        $event = null;
        try {
            $payload = @file_get_contents('php://input');
            $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );
        } catch(\UnexpectedValueException $e) {
            // Invalid payload
        } catch(\Stripe\Exception\SignatureVerificationException $e) {
            // Invalid signature
        } catch(\Exception $e) {
            // any error
        }
        return $event;

    }

}
