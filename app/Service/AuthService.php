<?php


namespace App\Service;


use App\Mail\EmailVerification;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class AuthService
{
    public function login($data)
    {
        // TODO: Implement login() method.
        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password'],'status'=>'activated'])) {

            $user = Auth::user();
            $token = $user->createToken('API')->accessToken;
            return $token;

        } else {
            return false;
        }
    }
    public function register($data)
    {
        $user =new User();
        $user->name=$data['name'];
        $user->email=$data['email'];
        $user->status='activated';
        $user->type='user';
        $user->password=bcrypt($data['password']);


        $user->save();

        return $user;
    }

}
