<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class GenerateLink extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $generatedLink;
    public  $user;
    public function __construct($link,$user)
    {
        $this->generatedLink=$link;
        $this->user=$user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('arsalanqayum332@gmail.com')
            ->subject("survey enrollment")
            ->view('link')->with('link',$this->generatedLink);
    }
}
