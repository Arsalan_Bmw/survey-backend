<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\Service\AuthService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    public $authservice;
    public function __construct(AuthService $authService)
    {
        $this->authservice  =   $authService;
    }
    public function login(LoginRequest $request)
    {
        $result =   $this->authservice->login($request->all());
        if($result)
        {
            return response()->json(['data'=>['error'=>false,'message'=>'logged In successfully','user'=>Auth::user(),'token'=>$result]],200);
        }
        else
        {
            return response()->json(['data'=>['error'=>true,'message'=>'Invalid credentials']],403);
        }
    }
    public function register(RegisterRequest $request)
    {
        $result = $this->authservice->register($request->all());
        if($result)
        {
            return response()->json(['data'=>['error'=>false,'message'=>'User register successfully','user'=>$result]],201);
        }
        else
        {
            return response()->json(['data'=>['error'=>true,'message'=>'Invalid credentials']],200);
        }

    }
    public function logout()
    {

        $result =  DB::table('oauth_access_tokens')
            ->where('user_id', Auth::user()->id)
            ->update([
                'revoked' => true
            ]);
        if($result)
        {
            return response()->json(['data'=>['error'=>false,'message'=>'logged Out successfully']],200);
        }


    }

}
