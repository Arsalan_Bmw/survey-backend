<?php

namespace App\Http\Controllers;

use App\GeneratedLink;
use App\Http\Requests\SurveyPaymentRequest;
use App\Mail\GenerateLink;
use App\StaticQuestionResult;
use App\Survey;
use App\SurveyPayment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SofortPaymentController extends Controller
{
    public function checkData(SurveyPaymentRequest $request)
    {
        return response()->json(['error'=>false,'message'=>'Valid data'],200);

    }
    public function payment(Request $request){
//        return $request->input('age');
        $result = Survey::where('id',$request->survey_id)->first();

        $user=new User();
        $user->name     =   $request->name;
        $user->email     =   $request->email;
        $user->status     =   'activated';
        $user->country     =   $request->country;
        $user->age     =   $request->age;
        $user->type     =   'user';
        $user->password     =   '';
        $user->save();

        $staticQUestion =   new StaticQuestionResult();
        $staticQUestion->user_id    =   $user->id;
        $staticQUestion->survey_id  = $request->survey_id;
        $staticQUestion->save();
        $survepayment   =   new SurveyPayment();
        $survepayment->payment   =   $request->amount;
        $survepayment->survey_id   =   $result->id;
        $survepayment->user_id   =   $user->id;
        $survepayment->save();

        $genratedCode   =   new GeneratedLink();
        $genratedCode->slug=$result->slug;
        $genratedCode->code=$request->code;
        $genratedCode->save();
        Mail::to($request->email)->send(new GenerateLink($genratedCode,$user->id));

        return response()->json(['error'=>false,'message'=>'Payment Made successfully','user'=>$user],200);

    }
}
