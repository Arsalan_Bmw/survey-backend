<?php

namespace App\Http\Controllers;

use App\GeneratedLink;
use App\Http\Requests\SurveyPaymentRequest;
use App\Http\Requests\SurveyRequest;
use App\Mail\GenerateLink;
use App\Question;

use App\Service\SurveyService;
use App\StaticQuestionResult;
use App\Survey;
use App\SurveyPayment;
use App\SurveyResult;
use App\User;
use Cartalyst\Stripe\Exception\StripeException;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Slim\Factory\AppFactory;

class SurveyController extends Controller
{
    /**
     * @var SurveyService
     */
    public $survey;

    /**
     * SurveyController constructor.
     * @param SurveyService $survey
     */
    public function __construct(SurveyService $survey)
    {
        $this->survey   = $survey;
    }
    public function atteptedSurvey($survey,$user)
    {
        $survey=Survey::where('slug',$survey)->first();

        $answer = DB::table('survey_results')->select('questions.question','surveys.title','survey_results.answer')
            ->join('questions','questions.id','=','survey_results.question_id')
            ->join('surveys','surveys.id','=','survey_results.survey_id')
           ->where('user_id',$user) ->get();
        return response()->json(['error'=>false,'message'=>'Result','data'=>$answer],200);

    }

    /**
     * return all survey
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $result =   $this->survey->get();
        if ($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Survey listing','data'=>$result]],200);

    }

    /**
     * create new survey
     * @param SurveyRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(SurveyRequest $request)
    {
        $result =   $this->survey->create($request->all());
        if($result)
        {
            return response()->json(['data'=>['error'=>false,'message'=>'Survey created successfully','data'=>$result]],201);
        }
        else
        {
            return response()->json(['data'=>['error'=>true,'message'=>'Oops, something went wrong']],403);
        }


    }
    public function getBySlug($slug)
    {
        $result =   $this->survey->getBySlug($slug);
        if($result)
        {
            return response()->json(['data'=>['error'=>false,'message'=>'Survey Listed','data'=>$result]],200);

        }
        else
        {
            return response()->json(['data'=>['error'=>true,'message'=>'Something went wrong','data'=>'']],200);
        }
    }

    /**
     * delete survey
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $question=Question::where('survey_id',$id)->count();
        if($question>0)
            return response()->json(['data'=>['error'=>true,'message'=>"Delete action can't be process.This survey has question"]],403);

        $result =   $this->survey->delete($id);
        if($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Survey deleted Successfully']],200);


    }

    /**
     * @param SurveyRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(SurveyRequest $request,$id)
    {
        $result =   $this->survey->update($request->all(),$id);
        if($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Survey updated Successfully']],200);
    }
    public function survey($id)
    {
        $result =   $this->survey->survey($id);
        if($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Survey Detail','data'=>$result]],200);
    }
    public function cardVerification(SurveyPaymentRequest $request){

        if(!$this->survey->survey($request->survey_id)){
            return response()->json(['error'=>true,'message'=>'Survey not exists'],404);
        }
        try {
             $user=new User();
             $user->name=$request->name;
             $user->email=$request->email;
             $user->status='activated';
             $user->type='user';
             $user->country=$request->country;
             $user->age=$request->age;
             $user->password='';
             $user->save();
            $staticQUestion =   new StaticQuestionResult();
            $staticQUestion->user_id    =   $user->id;
            $staticQUestion->survey_id  = $request->survey_id;
            $staticQUestion->save();

         $result=$this->survey->survey($request->survey_id);
        $stripe = Stripe::make('sk_test_51HFMS0I7k90FdVQTC8r1eMB1tbcoxBM9eI81bshghWFNyvGqsYEmRzAoBGdB2BZ1qse1Vlfi6VGEp3A74Ji3PoFu00DJ8mjYtS');
        $charge= $stripe->charges()->create([
           'amount'=>$result->fees,
           'currency'=>'USD',
           'source'=>$request->token,
           'description'=>$result->title,
           'receipt_email'=>$user->email,
           'metadata'=>[
               'data1'=>'yes',
               'data2'=>'yes',
               'data3'=>'yes',
           ]
       ]);
        $SurveyPayment  =new SurveyPayment();
        $SurveyPayment->survey_id  = $result->id;
        $SurveyPayment->user_id=$user->id;
        $SurveyPayment->payment=$result->fees;
        $SurveyPayment->save();
        $genratedCode   =   new GeneratedLink();
        $genratedCode->slug=$result->slug;
        $genratedCode->code=mt_rand();
        $genratedCode->save();
        Mail::to($request->email)->send(new GenerateLink($genratedCode,$user->id));

        return response()->json(['error'=>false,'message'=>'Payment Made successfully','user'=>$user],200);
        }catch (StripeException $exception){

            return response()->json(['error'=>true,'message'=>'Payment error'],401);
        }
    }
    public function createSession(){

        $stripe = new \Stripe\StripeClient(
            'sk_test_51HFMS0I7k90FdVQTC8r1eMB1tbcoxBM9eI81bshghWFNyvGqsYEmRzAoBGdB2BZ1qse1Vlfi6VGEp3A74Ji3PoFu00DJ8mjYtS'
        );
      $customer=  $stripe->customers->create([
            'description' => 'My First Test Customer (created for API docs)',
        ]);

        \Stripe\Stripe::setApiKey ('sk_test_51HFMS0I7k90FdVQTC8r1eMB1tbcoxBM9eI81bshghWFNyvGqsYEmRzAoBGdB2BZ1qse1Vlfi6VGEp3A74Ji3PoFu00DJ8mjYtS');
        $session = \Stripe\Checkout\Session::create([
            'success_url' => 'http://localhost:8080/',
            'cancel_url' => 'http://localhost:8080/',
            'payment_method_types' => ['card'],
            'line_items' => [
                [
                    'price_data' => [
                        'currency' => 'eur',
                        'unit_amount' => 2333,
                        'product_data' => [
                            'name' => 'test',
                        ],
                    ],
                    'quantity' => 1,
                ],
            ],
            'mode' => 'payment',
        ]);
            return response()->json([ 'id' => $session->id ],200);



    }
    public function checkPayment($slug){
        $result = $this->survey->getBySlug($slug);
        if($result){

                return response()->json(['error'=>true,'message'=>'Payment not made','data'=>$result],200);

        }else{
            return response()->json(['error'=>true,'message'=>'Resource not found'],401);
        }

    }
    public function getSurveyQuestion(Request $request,$slug)
    {
        $result =   $this->survey->getBySlug($slug);
        $surverResult   =   SurveyResult::where('survey_id',$result->id)
            ->where('user_id',$request->user)->get()->pluck('question_id');

        $question   =   Question::where('survey_id',$result->id)->whereNotIn('id', $surverResult)->inRandomOrder()->limit(1)->get();
        if($question && count($question)>0)
        {
            return response()->json(['error'=>false,'message'=>'Random question','data'=>$question],200);
        }else{
            return response()->json(['error'=>true,'message'=>'Survey completed'],403);
        }
    }
    public function enrolledInSurvey()
    {
        $surveyPayment= SurveyPayment::with('survey')->where('user_id',Auth::guard('api')->user()->id)->get();
        return response()->json(['error'=>false,'message'=>'Enrolled survey','data'=>$surveyPayment],200);
    }
    public function AllEnrolled()
    {
        $surveyPayment= SurveyPayment::with(['survey','user'])->get();
        return response()->json(['error'=>false,'message'=>'Enrolled survey','data'=>$surveyPayment],200);
    }
    public function surveyResult($user,$surveyId)
    {
        $surveyResult   =   DB::table('questions')
            ->join('survey_results','survey_results.question_id','=','questions.id')
            ->join('surveys','surveys.id','=','survey_results.survey_id')
            ->where('survey_results.survey_id',$surveyId)
            ->where('survey_results.user_id',$user)
            ->get();
        return response()->json(['error'=>false,'message survey result','data'=>$surveyResult],200);
    }
    public function checkPayments(Request $request)
    {
        $generateLinks=GeneratedLink::where('code',$request->code)->where('slug',$request->slug)->where('status',0)->count();

        if($generateLinks>0){
            return response()->json(['error'=>false,'message'=>'Payment verified'],200);
        }else{
            return response()->json(['error'=>true,'message'=>'Payment not verified'],200);
        }
    }
    public function updatecheckPayments(Request $request)
    {
        $generatedLinks=GeneratedLink::where('code',$request->code)
            ->where('slug',$request->slug)->first();
        $generatedLinks->status=1;
        $generatedLinks->save();
        return response()->json(['error'=>false,'message'=>'Survey Completed'],200);
    }


}
