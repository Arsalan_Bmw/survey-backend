<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QuestionAverageController extends Controller
{
    public function averageAnswer(){
        $result =   DB::select('select avg(answer) as average,category
from survey_results sr join questions q on sr.question_id = q.id group by category;
                       ');

        return response()->json(['error'=>false,'message'=>'Average Participant Answer','data'=>$result],200);
    }
}
