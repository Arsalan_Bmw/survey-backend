<?php

namespace App\Http\Controllers;

use App\CustomStart;
use App\GeneratedLink;
use App\Mail\GenerateLink;
use App\Question;
use App\Service\SurveyService;
use App\Service\StripeService;
use App\SurveyAttempt;
use App\SurveyAttemptResult;
use App\SurveyResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Mail;
use Countries;

class FrontendController extends Controller
{
    /**
     * @var SurveyService
     */
    public $survey;
    /**
     * @var StripeService
     */
    public $stripeService;

    /**
     * SurveyController constructor.
     * @param SurveyService $survey
     */
    public function __construct(SurveyService $survey, StripeService $stripeService)
    {
        $this->survey = $survey;
        $this->stripeService = $stripeService;
    }

    public function index()
    {
        return view('frontend.index', [
            "error" => request()->has('error')
        ]);
    }

    public function cookie(Request $request)
    {
        if($request->method() == "POST" and $request->has('accept')) {
            if($request->get('accept') == 1){
                return redirect()->route('index')->withCookie(cookie("accepted", 1, 10080));
            }
        }
        return view('frontend.cookie', [
        ]);
    }

    public function customStart($code)
    {
        if(CustomStart::where(['code' => $code])->first() instanceof CustomStart){
            $surveyAttempt = new SurveyAttempt();
            $surveyAttempt->code=mt_rand();
            $surveyAttempt->survey_id=1;
            $surveyAttempt->custom_code=$code;
            $surveyAttempt->save();
            return view('frontend.start', [
                'code' => $code,
                'surveyAttempt' => $surveyAttempt
            ]);
        }
        return redirect()->route('index')->with('danger', 'Leider konnten wir den gewünschten Zugangscode nicht finden.');
    }

    public function checkout()
    {
        $session = $this->stripeService->createNewCheckoutSession();
        return view('frontend.checkout', [
            'stripeSession' => $session
        ]);
    }

    public function success(Request $request)
    {
        $hash = $request->get('hash');
        $surveyAttemptId = decrypt($hash);
        $surveyAttempt = SurveyAttempt::find($surveyAttemptId);
        $surveyAttempt->paid_status = true;
        $surveyAttempt->save();

        return view('frontend.start', [
            'surveyAttempt' => $surveyAttempt
        ]);
//        dd($surveyAttempt->paid_status);
//        Mail::to($request->email)->send(new GenerateLink($genratedCode,$user->id));
    }

    public function question($code)
    {
        $surveyAttempt = SurveyAttempt::where(['code' => $code])->first();
        if(!$surveyAttempt instanceof SurveyAttempt){
            return redirect()->route('index')->with('danger', ' Leider existiert diese Befragung nicht.');
        }
        $countries = Countries::getList('de');
        $de = $countries['DE'];
        $at = $countries['AT'];
        $ch = $countries['CH'];
        $li = $countries['LI'];

        unset($countries['LI']);
        unset($countries['CH']);
        unset($countries['DE']);
        unset($countries['AT']);
        $firstCountries = [
            'AT' => $at,
            'DE' => $de,
            'CH' => $ch,
            'LI' => $li,
            '--' => '------------'
        ];
        $countries = $firstCountries + $countries;

        if($surveyAttempt->staticQuestionNotAnswered()){
            return view('frontend.starting_question', [
                'code' => $code,
                'countries' => $countries,
            ]);
        }

        $question = $this->getQuestionForAttempt($surveyAttempt);
        if(!$question instanceof Question){
            return redirect()->route('result', ['code' => $code]);
        }

        return view('frontend.question', [
            'question' => $question,
            'code' => $code
        ]);
    }

    private function getQuestionForAttempt(SurveyAttempt $surveyAttempt)
    {
        $answeredQuestionIds = SurveyAttemptResult::where('survey_attempt_id',$surveyAttempt->id)->get()->pluck('question_id');
        return Question::where('survey_id',$surveyAttempt->survey->id)->whereNotIn('id', $answeredQuestionIds)->inRandomOrder()->limit(1)->first();
    }

    public function answer(Request $request, $code){
        $surveyAttempt = SurveyAttempt::where(['code' => $code])->first();
        if(!$surveyAttempt instanceof SurveyAttempt){
            return redirect()->route('index');
        }
        if($request->has('question') and $request->has('answer')){
            $sar = new SurveyAttemptResult();
            $sar->question_id = $request->question;
            $sar->answer = $request->answer;
            $sar->survey_attempt_id = $surveyAttempt->id;
            $sar->save();
        }else{
            $request->validate([
                'gender' => ['required'],
                'age' => 'required|numeric|min:0|not_in:0',
                'country' => ['required'],
                'education' => ['required'],
                'occupation' => ['required'],
            ], [
                'gender.required' => 'Bitte wähle dein Geschlecht',
                'age.required' => "Bitte wähle dein Alter",
                'age.min' => "Sie müssen einen positiven Wert für das Alter angeben.",
                'country.required' => "Bitte wähle dein Land",
                'education.required' => "Bitte wähle dein höchsten Bildungsabschluss",
                'occupation.required' => "Bitte wähle deine derzeitige Beschäftigung",
            ]);
            $surveyAttempt->gender = $request->gender;
            $surveyAttempt->age = $request->age;
            $surveyAttempt->country = $request->country;
            $surveyAttempt->education = $request->education;
            $surveyAttempt->occupation = $request->has('occupation') ? implode(',',$request->occupation) : '';
            $surveyAttempt->save();
        }

        return redirect()->route('question', ['code' => $code]);
    }

    public function error()
    {
        return view('frontend.error');
    }

    public function result(Request $request, $code)
    {
        $surveyAttempt = SurveyAttempt::where(['code' => $code])->first();
        if(!$surveyAttempt instanceof SurveyAttempt){
            return redirect()->route('index');
        }
        $data = ['my' => [],'all' => []];
        $cols = ['complexity','invasion','uncertainty','privacy','overload','safety','social','usefulness','support','unreliability'];
        // counting all results
        $all = SurveyAttemptResult::with('question')->get();

        if($surveyAttempt->cutom_code != null){
            $surveyIds = SurveyAttempt::where('custom_code', $surveyAttempt->cutom_code)->pluck('id')->toArray();
            $all = SurveyAttemptResult::whereIn('survey_attempt_id',$surveyIds)->with('question')->get();
        }
        $help = [];
        foreach($all as $result){
            if(!isset($help[$result->question->category])){
                $help[$result->question->category] = [
                    'answerSum' => 0,
                    'answerCount' => 0,
                ];
            }
            $help[$result->question->category]['answerSum'] += $result->answer;
            $help[$result->question->category]['answerCount']++;
        }

        foreach($cols as $col) {
            if (isset($help[$col])) {
                $data['all'][$col] = $help[$col]['answerSum'] / $help[$col]['answerCount'];
            }
        }

        // counting my results
        $results = SurveyAttemptResult::where('survey_attempt_id', $surveyAttempt->id)->with('question')->get();
        $help = [];
        foreach($results as $result){
            if(!isset($help[$result->question->category])){
                $help[$result->question->category] = [
                    'answerSum' => 0,
                    'answerCount' => 0,
                ];
            }
            $help[$result->question->category]['answerSum'] += $result->answer;
            $help[$result->question->category]['answerCount']++;
        }

        foreach($cols as $col) {
            if (isset($help[$col])) {
                $data['my'][$col] = $help[$col]['answerSum'] / $help[$col]['answerCount'];
            }
        }

        return view('frontend.result', [
            'code' => $code,
            'data' => $data,
            'cols' => $cols,
        ]);
    }

    public function continue(Request $request)
    {
        if($request->method() == "POST"){
            $code = $request->post('code');
            if($code){
                $surveyAttempt = SurveyAttempt::where(['code' => $code])->first();
                if($surveyAttempt instanceof SurveyAttempt){
                    $question = $this->getQuestionForAttempt($surveyAttempt);
                    if(!$question instanceof Question){
                        return redirect()->route('index')->with('success', 'Sie haben bereits die Befragung erfolgreich abgeschlossen.');
                    }

                    return redirect()->route('question',['code' => $code]);
                }
            }
            return redirect()->route('continue')->with('danger', 'Der Code existiert leider nicht.');
        }
        return view('frontend.continue');
    }

    public function exportCsv(Request $request, $access, $code = null)
    {
        if($access != 'DigiStress20Riedl'){
            return "";
        }
        $fileName = sprintf('export%s.csv',($code ? '-'.$code : ''));
        $attempts = SurveyAttempt::all();
        if($code != null){
            $attempts = SurveyAttempt::where('custom_code',$code)->get();
        }

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('Zugangscode', 'Organisationscode', 'paid_status', 'created_at', 'gender', 'age','country','education','occupation');
        $questions = Question::where(['survey_id' => 1])->get();
        $count = 1;
        foreach($questions as $question){
            array_push($columns, $question->question);
            $count ++;
        }
        $educations = SurveyAttempt::educationArray();
        $occupations = SurveyAttempt::occupationArray();
        $callback = function() use($attempts, $columns, $educations, $occupations) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);
            foreach ($attempts as $attempt) {
                $row['Zugangscode']  = $attempt->code;
                $row['Organisationscode']  = $attempt->custom_code;
                $row['paid_status']    = $attempt->paid_status;
                $row['created_at']    = $attempt->created_at;
                $row['gender']  = $attempt->gender;
                $row['age']  = $attempt->age;
                $row['country']  = $attempt->country;
                $row['education']  = isset($educations[$attempt->education]) ? $educations[$attempt->education] : $attempt->education;
                $row['occupation'] = $attempt->getOccupationsAsString();

                /** @var SurveyAttemptResult $answer */
                foreach($attempt->answers as $answer){
                    $row['question-'.$answer->question->id]  = $answer->answer;
                }
                fputcsv($file, $row);
            }

            fclose($file);
        };

        return response()->stream($callback, 200, $headers);
    }

    public function imprint()
    {
        return view('frontend.imprint');
    }

}
