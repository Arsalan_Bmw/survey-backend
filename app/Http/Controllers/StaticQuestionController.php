<?php

namespace App\Http\Controllers;

use App\StaticQuestionResult;
use App\StatisQuestions;
use Illuminate\Http\Request;

class StaticQuestionController extends Controller
{
    public function index(){
        $result =StatisQuestions::get();
        return response()->json(['error'=>false,'message'=>'Static Question Listing','data'=>$result],200);
    }
    public function store(Request $request){

        $answer = StaticQuestionResult::where('user_id',$request->user_id)
            ->where('survey_id',$request->survey_id)
            ->where('attempted','0')->first();
        $answer->user_id    =   $request->user_id;
        $answer->survey_id    =   $request->survey_id;
        $answer->answer_one    =   $request->answer_one;
        $answer->answer_two    =   $request->answer_two;
        $answer->answer_three    =   $request->answer_three;
        $answer->answer_four    =   $request->answer_four;
        $answer->answer_five   =   $request->answer_five;
        $answer->attempted   = '1';
        $answer->save();
        return response()->json(['error'=>false,'message'=>"Question submitted successfully"],201);

    }
    public function getAttemptedQuestion(Request $request){
        $question   =   StaticQuestionResult::where('user_id',$request->user_id)
            ->where('survey_id',$request->survey_id)
            ->where('attempted','0')
            ->get();
        return response()->json(['error'=>false,'message'=>'Static question','data'=>$question],200);
    }
}
