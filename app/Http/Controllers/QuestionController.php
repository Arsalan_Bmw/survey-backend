<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Question;
use App\Service\QuestionService;
use App\Survey;
use App\SurveyResult;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class QuestionController extends Controller
{
    /**
     * @var QuestionService
     */
    public $questionService;

    /**
     * QuestionController constructor.
     * @param QuestionService $questionService
     */
    public function __construct(QuestionService $questionService)
    {
        $this->questionService  =   $questionService;
    }
    public function index()
    {
        $result =   $this->questionService->get();
        if ($result)
            return response()->json(['error'=>false,'message'=>'Questions listing','data'=>$result],200);
    }

    /**
     * create question
     * @param QuestionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(QuestionRequest $request)
    {
        $result = $this->questionService->create($request->all());
        if ($result)
            return response()->json(['error'=>false,'message'=>'Quation Created'],201);
        else
            return response()->json(['error'=>true,'message'=>'Something went wrong'],403);


    }
    public function delete($id)
    {
        $result = $this->questionService->delete($id);
        if($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Question deleted successfully']],200);

    }
    public function update(QuestionRequest $request,$id)
    {
        $result =   $this->questionService->update($request->all(),$id);
        if($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Question updated successfully']],200);
    }
    public function question($id)
    {
        $result = $this->questionService->question($id);
        if($result)
            return response()->json(['data'=>['error'=>false,'message'=>'Question Detail','data'=>$result]],200);

    }
    public function postAnswer(Request $request)
    {
        $surveyResult   =   new SurveyResult();
        $surveyResult->survey_id    = $request->survey_id;
        $surveyResult->answer    = $request->answer;
        $surveyResult->user_id      =   $request->user_id;
        $surveyResult->question_id     = $request->question_id;
        $surveyResult->save();
        return response()->json(['error'=>false,'message'=>'Answer submitted'],200);
    }

}
