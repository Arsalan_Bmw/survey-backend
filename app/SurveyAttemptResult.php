<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyAttemptResult extends Model
{
    public function question()
    {
        return $this->belongsTo(Question::class,'question_id','id');
    }
}
