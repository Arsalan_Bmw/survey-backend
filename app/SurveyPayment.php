<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyPayment extends Model
{
    public function survey()
    {
        return $this->belongsTo(Survey::class,'survey_id','id');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
