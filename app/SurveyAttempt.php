<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyAttempt extends Model
{
    public $guarded = [];

    public function survey()
    {
        return $this->belongsTo(Survey::class,'survey_id','id');
    }

    public function answers()
    {
        return $this->hasMany(SurveyAttemptResult::class,'survey_attempt_id','id');
    }

    public function staticQuestionNotAnswered()
    {
        return $this->gender === null or $this->age === null or $this->country === null or $this->education === null or $this->occupation === null;
    }

    public static function educationArray()
    {
        return [
            1 => "Kein Abschluss",
            2 => "Pflichtschulabschluss",
            3 => "Lehre / Berufsausbildung",
            4 => "Fachhochschul- oder Hochschulreife (= Abitur, Matura)",
            -1 => "Will ich nicht sagen",
        ];
    }
    public static function occupationArray()
    {
        return [
            1 => "Angestellter (auch Teilzeit)",
            2 => "Arbeiter (auch Teilzeit)",
            3 => "Selbständiger",
            4 => "Studierender",
            5 => "Ohne Beschäftigung",
            6 => "Will ich nicht sagen",
        ];
    }

    public function getOccupationsAsString()
    {
        $help = [];
        $occupations = self::occupationArray();
        foreach(explode(",",$this->occupation) as $occupation){
            $help[]= isset($occupations[$occupation]) ? $occupations[$occupation] : $occupation;
        }

        return implode(', ', $help);
    }
}
