<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}" type="image/vnd.microsoft.icon" />
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

{{--    {{ dd('seo.'.\Request::route()->getName()) }}--}}
    <title>
        @if(isset($pageTitle))
            {{$pageTitle}}
        @endif
    </title>
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans&display=swap" rel="stylesheet">
    <!-- Styles -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    @yield('css')
</head>
<body>
<div>
{{--    <header>--}}
{{--    </header>--}}
    <main class="container">
        <div class="col-6 offset-3">
            <div id="flash-messages" class="{{ (isset(Session::all()['_flash']) and count(Session::all()['_flash']['old']) > 0) ? 'show' :'' }}">
                @foreach (['danger', 'warning', 'success', 'info'] as $msg)
                    @if(Session::has($msg))
                        <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}<i class="fas fa-times"></i></p>
                    @endif
                @endforeach

                @foreach ($errors->all() as $error)
                    <p class="alert alert-danger">{{ $error }} <i class="fas fa-times"></i></p>
                @endforeach
            </div>
        </div>
        @yield('content')
    </main>
</div>
<footer class="container mt-5">
    <div class="row">
        <div class="col-md-6">
            <strong>© FH OÖ Forschungs und Entwicklungs GmbH</strong> <br>
            Roseggerstraße 15 <br>
            4600 Wels, Austria
        </div>
        <div class="col-md-6 text-right">
            <strong>Links</strong><br>
            <a href="{{ route('imprint') }}" target="_blank">Impressum</a><br>
            <a href="{{ route('continue') }}" target="_blank">Mit Code fortsetzen</a><br>
            <a href="http://digital-stress.info/" target="_blank">digital-stress.info</a>
        </div>
    </div>

</footer>

<script src="{{ mix('/js/app.js') }}"></script>

@yield('scripts')

</body>
</html>
