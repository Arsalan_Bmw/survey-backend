@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="shadow-box text-center col-md-8 offset-md-2">
        <h1>Impressum</h1>
        <p class="mt-5" >OFFENLEGUNGSPFLICHT GEMÄSS §25 MEDIENGESETZ UND INFORMATIONSPFLICHT LT. § 5 ABS. 1 E-COMMERCE GESETZ
            MEDIENINHABER UND HERAUSGEBER
        </p>
        <div class="text-left">
            Research Center der FH OÖ, Campus Steyr <br>
            Wehrgrabengasse 1-3<br>
            A-4400 Steyr<br>
            Österreich<br>
            <br>
            Telefon
            +43 5 0804 33452<br>
            Fax
            +43 5 0804 33499<br>
            E-Mail
            michaela.obermayr@fh-steyr.at<br>
            <br><br>
            <strong>RECHNUNGSANSCHRIFT</strong><br>
            FH OÖ Forschungs- und Entwicklungs GmbH<br>
            Roseggerstraße 15<br>
            4600 Wels<br>
            Austria<br>
            <br>
            FB Nr. 236733m<br>
            UID Nr. ATU 57300236<br>
        </div>

    </div>
@endsection

@section('scripts')
@endsection
