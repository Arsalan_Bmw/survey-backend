@extends('layout')

@section('title', 'Digitaler Stress' )

@section('css')

@endsection
@section('content')
    <div class="shadow-box col-md-12">
        <div class="text-center">
            <h1>Ihr Ergebnis</h1>
            <div class="alert alert-info col-8 offset-2">Ihr Ergebnis wird Ihnen nur einmal angezeigt, danach können Sie
                Ihr Ergebnis nicht mehr einsehen. Sie können sich Ihr Ergebnis mittels dem Button "Ergebnis drucken" ausdrucken.</div>
            <a href="#" onclick="window.print();" class="btn btn-primary no-print" style="position: absolute; top: 10px; right:10px;">Ergebnis drucken</a>
            <a href="#" class="close-results btn btn-outline-primary no-print" style="position: absolute; top: 10px; left:10px;">Schließen</a>
        </div>
        <div class="col-10 offset-1">
            <canvas id="canvas"></canvas>
        </div>
    </div>
    <div class="shadow-box col-md-12 mt-5 ">
    <h1 class=" text-center">Erläuterungen</h1>
        <div class="row mt-5">
            <div class="col-md-6">
                <h4>Komplexität <span class="only-print">({{$data['my']['complexity']}})</span></h4>
                Je höher der Wert, desto ausgeprägter die <strong><i>Komplexität</i></strong>. Ein Benutzer fühlt sich von Informations- und Kommunikationstechnologien überfordert. Die Anforderungen, um das System bedienen zu können, überschreiten die Fähigkeiten.
                @include('frontend.progress', ['value' => $data['my']['complexity']])
            </div>
            <div class="col-md-6">
                <h4>Gestörte Work-Life Balance <span class="only-print">({{$data['my']['invasion']}})</span></h4>
                Je höher der Wert, desto <strong><i>Gestörter die Work-Life Balance</i></strong>. Ein Mitarbeiter ist ständig erreichbar und fühlt sich verpflichtet, jederzeit für Kollegen und Vorgesetzte verfügbar zu sein.
                @include('frontend.progress', ['value' => $data['my']['invasion']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Sorge, durch Technologie ersetzt zu werden <span class="only-print">({{$data['my']['uncertainty']}})</span></h4>
                Je höher der Wert, desto ausgeprägter die <strong><i>Sorge, durch Technologie ersetzt zu werden</i></strong>. Die Ungewissheit ist insbesondere dann ausgeprägt, wenn folgende Gefahren bestehen: durch vermehrten Einsatz von Technologie und/oder durch Personen, die ein besseres Verständnis von Technologie haben, ersetzt zu werden, was mit dem Verlust des Arbeitsplatzes einhergeht.
                @include('frontend.progress', ['value' => $data['my']['uncertainty']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Verletzung der Privatsphäre <span class="only-print">({{$data['my']['privacy']}})</span></h4>
                Je höher der Wert, desto ausgeprägter ist die <strong><i>Verletzung der Privatsphäre</i></strong>. Diese leidet dann, wenn befürchtet wird, dass Informationen für unbefugte Dritte leicht zugänglich sind und persönliche Daten gestohlen werden könnten.
                @include('frontend.progress', ['value' => $data['my']['privacy']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Überlastung mit Informationen und Aufgaben <span class="only-print">({{$data['my']['overload']}})</span></h4>
                Je höher der Wert von <strong><i>Überlastung mit Informationen und Aufgaben</i></strong>, desto mehr führt die Nutzung digitaler Technologien dazu, dass man mehr Aufgaben in immer kürzerer Zeit abzuarbeiten hat. Zudem kann es aufgrund der Nutzung vieler Medien zu Informationsüberlastung kommen.
                @include('frontend.progress', ['value' => $data['my']['overload']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Probleme mit der Sicherheit <span class="only-print">({{$data['my']['safety']}})</span></h4>
                Je höher der Wert, desto mehr <strong><i>Probleme mit der Sicherheit</i></strong> bestehen. Benutzer sorgen sich, schadhafte Programme herunterzuladen oder Opfer von Hackerangriffen zu werden. Zudem können E-Mails unbekannter Absender Bedenken auslösen.
                @include('frontend.progress', ['value' => $data['my']['safety']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Sozialer Druck und Kommunikationsmisere <span class="only-print">({{$data['my']['social']}})</span></h4>
                Je höher der Wert, desto ausgeprägter <strong><i>sozialer Druck und Kommunikationsmisere</i></strong>. Es besteht das Gefühl, jederzeit und überall für andere erreichbar sein zu müssen. Die Technologienutzung bewirkt ungewollte soziale Normen wie die Erwartung, dass E-Mails direkt beantwortet werden müssen.
                @include('frontend.progress', ['value' => $data['my']['social']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Mangelnde Nützlichkeit <span class="only-print">({{$data['my']['usefulness']}})</span></h4>
                Je höher der Wert, desto ausgeprägter die <strong><i>Mangelnde Nützlichkeit</i></strong> von Programmen und Informationssystemen. Die genutzten Systeme haben einerseits viele Funktionen, die keinen Beitrag zur Erfüllung der betrieblichen Aufgaben leisten, andererseits werden aber wichtige Funktionen nicht angeboten.
                @include('frontend.progress', ['value' => $data['my']['usefulness']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Mangel an Support <span class="only-print">({{$data['my']['support']}})</span></h4>
                Je höher der Wert, desto ausgeprägter der <strong><i>Mangel an Support</i></strong>. Es steht bei technischen Problemen keine ausreichende Unterstützung zur Verfügung und/oder es dauert lange, bis Abhilfe geschaffen wird.
                @include('frontend.progress', ['value' => $data['my']['support']])
            </div>
            <div class="col-md-6 mt-4">
                <h4>Unzuverlässigkeit und technische Störungen <span class="only-print">({{$data['my']['unreliability']}})</span></h4>
                Je höher der Wert, desto ausgeprägter sind die <strong><i>Unzuverlässigkeit und technische Störungen</i></strong>. Systemabstürze oder lange Antwortzeiten sind typische Probleme.
                @include('frontend.progress', ['value' => $data['my']['unreliability']])
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" integrity="sha512-s+xg36jbIujB2S2VKfpGmlC3T5V2TF3lY48DX7u2r9XzGzgPsa6wTpOQA7J9iffvdeBN0q9tKzRxVxw1JviZPg==" crossorigin="anonymous"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js" ></script>
    <script>
        $('.close-results').on('click',function(){
            var r = confirm("Die Seite lässt sich nicht mehr öffnen, wenn Sie diese geschlossen haben.");
            if(r){
                window.location ="/";
            }
        });
        var config = {
            type: 'line',
            data: {
                labels: ['Komplexität','Gestörte Work-Life Balance','Sorge, durch Technologie ersetzt zu werden','Verletzung der Privatsphäre','Überlastung mit Informationen und Aufgaben','Probleme mit der Sicherheit','Sozialer Druck und Kommunikationsmisere','Mangelnde Nützlichkeit','Mangel an Support','Unzuverlässigkeit und technische Störungen'],
                datasets: [{
                    label: 'Meine Ergebnisse',
                    backgroundColor: "#b11226",
                    borderColor: "#b11226",
                    data: [
                        {{ implode(',',array_values($data['my'])) }}
                    ],
                    fill: false,
                }, {
                    label: 'Ergebnisse Gesamt',
                    fill: false,
                    backgroundColor: window.chartColors.blue,
                    borderColor: window.chartColors.blue,
                    data: [
                        {{ implode(',',array_values($data['all'])) }}
                    ],
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                },
                legend: {
                    onClick: (e) => e.stopPropagation()
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: false,
                            // labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        ticks: {
                            beginAtZero: false,
                            max: 7,
                            min: 1,
                            steps: 6,
                            stepValue: 1,
                            callback: function(value) {
                                if (value % 1 === 0)
                                {
                                    if(value == 1){
                                        return "Kein Stress   1";
                                    }else if(value == 7){
                                        return "Sehr hoher Stress  7";
                                    }
                                    return value;
                                }
                            }
                        },
                        scaleLabel: {
                            // display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('canvas').getContext('2d');
            window.myLine = new Chart(ctx, config);
        };
    </script>
@endsection
