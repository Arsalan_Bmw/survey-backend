@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="text-center">
        <h1>Digitaler Stress</h1>
        <p mt-4>Sie werden zur Zahlung weiter geleitet.</p>
    </div>
@endsection

@section('scripts')
    <script src="https://js.stripe.com/v3/"></script>
    <script>
        var stripe = Stripe('{{env('STRIPE_KEY')}}');
        stripe.redirectToCheckout({
            sessionId: '{{ $stripeSession->id }}'
        }).then(function (result) {
            alert(result);
        });
    </script>
@endsection
