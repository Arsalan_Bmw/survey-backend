@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="shadow-box">
        <div class="text-center">
            <h3>{{ $question->question }}</h3>
            <p class="mt-5">Welche Antwort trifft auf dich zu?</p>
        </div>
        <div class="text-center">
            <form action="{{ route('answer', ['code' => $code]) }}">
                <input type="hidden" name="question" value="{{ $question->id }}">
                <table class="mt-5" border="0">
                    <tr>
                        <td width="14%">Stimme überhaupt nicht zu</td>
                        <td width="14%"></td>
                        <td width="14%"></td>
                        <td width="14%">Neutral</td>
                        <td width="14%"></td>
                        <td width="14%"></td>
                        <td width="14%">Stimme völlig zu</td>
                    </tr>
                    <tr>
                        <td><input type="radio" value="1" name="answer"/></td>
                        <td><input type="radio" value="2" name="answer"/></td>
                        <td><input type="radio" value="3" name="answer"/></td>
                        <td><input type="radio" value="4" name="answer" required/></td>
                        <td><input type="radio" value="5" name="answer"/></td>
                        <td><input type="radio" value="6" name="answer"/></td>
                        <td><input type="radio" value="7" name="answer"/></td>
                    </tr>
                </table>
                <div class="text-center">
                    <button type="submit" style="display: none;" class="btn btn-primary mt-3 btn-sm">Antwort speichern
                        und weiter
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('input[name=answer]').on('change', function () {
                $('button[type=submit]').fadeIn();
            });
        });
    </script>
@endsection
