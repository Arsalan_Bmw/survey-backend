@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="text-center shadow-box col-md-8 offset-md-2">
        <h1>Digitaler Stress</h1>
        <p class="mt-4" >Starte jetzt deine Befragung. Bitte notiere dir deinen persönlichen Zugangscode, um später
            die Befragung fortzusetzen.</p>

        <h3>{{ $surveyAttempt->code }}</h3>
        <div class="action mt-3 text-center">
            <a href="{{ route('question',['code' => $surveyAttempt->code ]) }}" class="btn btn-primary btn-lg">Jetzt starten</a>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
