@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="shadow-box text-center col-md-8 offset-md-2">
        <h1>Digitaler Stress</h1>
        <p class="mt-5" >Die Teilnahme an der Befragung kostet 1,99 Euro (inkl. 10% MwSt.). Starte jetzt den sicheren Bezahlvorgang über
            <a href="https://stripe.com" target="_blank">Stripe</a> mit einem Klick auf den Button und führe anschließend die Befragung durch.</p>
        <div class="action mt-5 text-center mb-5">
            <a href="{{ route('checkout') }}" class="btn btn-primary btn-lg">Jetzt an der Befragung teilnehmen</a>
        </div>

    </div>
@endsection

@section('scripts')
@endsection
