@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="text-center shadow-box col-md-8 offset-md-2">
        <h1>Digitaler Stress</h1>
        <p class="mt-4" >Du hast deinen persönlichen Zugangscode und möchtest die Befragung fortsetzen.
        Dann gib' deinen persönlichen Code hier ein, um die Befragung fortzusetzen.
        </p>
        <div class="col-md-6 offset-3">
            <form action="{{ route('continue') }}" method="post">
                @csrf
                <input type="text" name="code" placeholder="" class="form-control" required>
                <button class="btn btn-primary mt-2" type="submit">Weiter machen oder Ergebnis ansehen</button>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
@endsection
