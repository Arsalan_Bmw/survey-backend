@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')

    <div class="text-center shadow-box col-md-12 ">
        <div class="text-center mb-4">
            <h1>Digitaler Stress</h1>
            <p>Bitte geben Sie im Folgenden Ihre Zustimmung zu den Aussagen an. IKT steht im Fragebogen für
                Informations- und Kommunikationstechnologie(n) und umfasst im weitesten Sinne alle Anwendungssysteme,
                Programme, Software, Hardware, IT-Geräte wie PC, Tablet, Smartphone usw. Sobald Sie den jeweiligen
                Zustimmungsgrad zu den 50 Aussagen zum digitalen Stress angegeben haben, erhalten Sie am Ende den Befund zu Ihrem aktuellen
                Digital-Stress-Wert. Der Wert gibt an, wie hoch Ihr Stress im Kontext der Verwendung und Allgegenwart
                digitaler Technologien am Arbeitsplatz ist. Der Wertebereich liegt zwischen 1 (= kein Stress) und 7 (=
                sehr hoher Stress).</p>
            <div class="col-6 offset-3">
{{--                <div id="flash-messages"--}}
{{--                     class="{{ (isset(Session::all()['_flash']) and count(Session::all()['_flash']['old']) > 0) ? 'show' :'' }}">--}}
{{--                    @foreach (['danger', 'warning', 'success', 'info'] as $msg)--}}
{{--                        @if(Session::has($msg))--}}
{{--                            <p class="alert alert-{{ $msg }}">{{ Session::get($msg) }}<i class="fas fa-times"></i></p>--}}
{{--                        @endif--}}
{{--                    @endforeach--}}

{{--                    @foreach ($errors->all() as $error)--}}
{{--                        <p class="alert alert-danger">{{ $error }} <i class="fas fa-times"></i></p>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-8 offset-2 text-left">
                <form action="{{ route('answer', ['code' => $code]) }}">
                    <div class="form-group row">
                        <label for="gender" class="col-4 col-form-label">Geschlecht</label>
                        <div class="col-8">
                            <select id="gender" name="gender" class="form-control" required>
                                <option value="x">Will ich nicht sagen</option>
                                <option value="m">Weiblich</option>
                                <option value="f">Männlich</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="age" class="col-4 col-form-label">Alter</label>
                        <div class="col-8">
                            <input type="number" class="form-control" id="age" name="age"  required>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="country" class="col-4 col-form-label">Land</label>
                        <div class="col-8">
                            <select id="gender" name="country" class="form-control" required>
                                @foreach($countries as $iso => $country)
                                <option value="{{$iso}}">{{ $country }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="education" class="col-4 col-form-label">Höchster Bildungsabschluss</label>
                        <div class="col-8">
                            <select id="education" name="education" class="form-control" required>
                                <option value="1">Kein Abschluss</option>
                                <option value="2">Pflichtschulabschluss</option>
                                <option value="3">Lehre / Berufsausbildung</option>
                                <option value="4">Fachhochschul- oder Hochschulreife (= Abitur, Matura)</option>
                                <option value="5">Fachhochschul- oder Hochschulabschluss</option>
                                <option value="-1">Will ich nicht sagen</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="ocupation" class="col-4 col-form-label">Derzeitige Beschäftigung</label>
                        <div class="col-8">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="1" name="occupation[]"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Angestellter (auch Teilzeit)
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="2" name="occupation[]"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Arbeiter (auch Teilzeit)
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="3" name="occupation[]"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Selbständiger
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="4" name="occupation[]"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Studierender
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="5" name=occupation[]"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Ohne Beschäftigung
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="6" name="occupation[]"
                                       id="defaultCheck1">
                                <label class="form-check-label" for="defaultCheck1">
                                    Will ich nicht sagen
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary mt-3 ">Antwort speichern und starten</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>
        $(function () {
            $('input[name=answer]').on('change', function () {
                $('button[type=submit]').fadeIn();
            });
        });
    </script>
@endsection
