@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="shadow-box text-center col-md-8 offset-md-2">
        <h1>Digitaler Stress</h1>
        <p class="mt-5" >Sie haben sich dazu entschieden, ein wissenschaftlich entwickeltes Online-Befragungs-Tool zu verwenden, mit dem Sie Ihren digitalen Stress messen können. Nachdem Sie alle Fragen beantwortet haben, erhalten Sie am Ende einen Gesamtwert zu Ihrem digitalen Stress. Der Wertebereich liegt zwischen 1 (= kein Stress) und 7 (= sehr hoher Stress). Die folgende Befragung ist anonym. Neben Angaben zum digitalen Stress werden zu wissenschaftlichen Forschungszwecken Geschlecht, Alter, Herkunftsland, höchster Bildungsabschluss und Ihre derzeitige Beschäftigung abgefragt. Durch die Nutzung des Online-Befragungs-Tools stimmen Sie der Verarbeitung und Speicherung Ihrer Daten zu. Die Daten sind eine Grundlage für wissenschaftliche Analysen und Forschungen und werden unter Einhaltung gesetzlicher Vorschriften verarbeitet und gespeichert.
        </p>
        <form action="{{ route('cookie') }}" class="text-left" method="post">
            @csrf
            <div class="form-group row">
                <div class="col-8 offset-2">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="1" name="accept"
                               id="accept1">
                        <label class="form-check-label" for="defaultCheck1">
                            Ich stimme der Verarbeitung und Speicherung meiner Befragungsdaten zu.
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" value="0" name="accept"
                               id="accept1">
                        <label class="form-check-label" for="defaultCheck1">
                            Ich stimme der Verarbeitung und Speicherung meiner Befragungsdaten <strong>NICHT</strong> zu.
                        </label>
                    </div>
                </div>
            </div>
            <div class="action mt-5 text-center mb-5">
                <button type="submit" class="btn btn-primary btn-lg">Speichern</button>
            </div>
        </form>

    </div>
@endsection

@section('scripts')
@endsection
