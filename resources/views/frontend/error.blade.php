@extends('layout')

@section('title', 'Digitaler Stress' )

@section('content')
    <div class="text-center">
        <h1>Digitaler Stress</h1>
        <div class="text-center">
            <div class="alert alert-danger d-inline-block mt-3">
                Leider ist etwas bei der Bezahlung schief gelaufen, versuche es erneut.
            </div>
        </div>
    </div>
    <div class="action mt-2 text-center">
        <a href="/" class="btn btn-primary btn-lg">Zur Startseite</a>
    </div>
@endsection

@section('scripts')
@endsection
