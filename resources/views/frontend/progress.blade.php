<div style="width:{{ $value/7*100 }}%" class="progress mt-2 no-print">
    <div class="text-right">
        {{$value}}
    </div>
</div>
