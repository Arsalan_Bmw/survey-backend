<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \App\Http\Controllers\FrontendController;

Route::get('/', function () {
    return view('link');
});
Route::match(['get','post'], '/cookie', [FrontendController::class, 'cookie'])->name('cookie');

Route::group(['middleware'
=> ['cookieCheck']], function(){
    Route::get('/', [FrontendController::class, 'index'])->name('index');
    Route::get('/s/{code}', [FrontendController::class, 'customStart'])->name('custom.start');
    Route::get('/jetzt-starten', [FrontendController::class, 'checkout'])->name('checkout');
    Route::get('/los', [FrontendController::class, 'success'])->name('success');
    Route::get('/fehler', [FrontendController::class, 'error'])->name('error');
    Route::get('/start', [FrontendController::class, 'start'])->name('start');
    Route::any( '/code', [FrontendController::class, 'continue'])->name('continue');
    Route::get('/frage/{code}', [FrontendController::class, 'question'])->name('question');
    Route::get('/antwort/{code}', [FrontendController::class, 'answer'])->name('answer');
    Route::get('/ergebnis/{code}', [FrontendController::class, 'result'])->name('result');
    Route::get('/e/{access}/{code?}', [FrontendController::class, 'exportCsv'])->name('export');
});
Route::get('/impressum', [FrontendController::class, 'imprint'])->name('imprint');
