<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('stripe-session','SurveyController@createSession');
Route::group(['prefix'=>'v1'],function(){
    Route::get('user/{id}',function($id){
       $user    =   \App\User::where('id',$id)->first();
       return response()->json(['error'=>false,'message'=>'User','data'=>$user],200);
    });
   Route::group(['prefix'=>'auth'],function (){
       Route::post('login','API\AuthController@login');
       Route::post('logout', 'API\AuthController@logout')->middleware('auth:api');
       Route::post('register','API\AuthController@register');
   }) ;

   Route::group(['prefix'=>'survey'],function(){
       Route::group(['prefix'=>'sofort'],function(){
           Route::post('/','SofortPaymentController@checkData');
           Route::post('payment','SofortPaymentController@payment');
       });
      Route::get('answer/{slug}/{user}','SurveyController@atteptedSurvey');
      Route::post('/','SurveyController@create');
      Route::get('/','SurveyController@index');
      Route::get('enrolled-survey','SurveyController@enrolledInSurvey');
      Route::get('enrolled','SurveyController@AllEnrolled');
      Route::get('result/{user}/{id}','SurveyController@surveyResult');
      Route::get('question/{slug}','SurveyController@getSurveyQuestion');
      Route::post('check-survey-payment','SurveyController@checkPayments');
      Route::post('update-survey-payment','SurveyController@updatecheckPayments');
      Route::get('/{id}','SurveyController@survey');
      Route::get('get-by-slug/{id}','SurveyController@getBySlug');
      Route::get('check-payment/{slug}','SurveyController@checkPayment');
      Route::delete('/{id}','SurveyController@delete');
      Route::put('/{id}','SurveyController@update');
      Route::post('check','SurveyController@cardVerification');
      Route::put('payment','SurveyController@cardVerification');
   });
    Route::get('static-question','StaticQuestionController@index');
    Route::post('static-question','StaticQuestionController@store');
    Route::post('check-attempt','StaticQuestionController@getAttemptedQuestion');
    Route::get('average-answer','QuestionAverageController@averageAnswer');
   Route::group(['prefix'=>'question'],function(){
       Route::post('/','QuestionController@create');
       Route::post('answer','QuestionController@postAnswer');
       Route::get('/','QuestionController@index');
       Route::get('/{id}','QuestionController@question');
       Route::delete('/{id}','QuestionController@delete');
       Route::put('/{id}','QuestionController@update');
   });
});
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
