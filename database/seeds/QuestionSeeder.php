<?php

use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Question::truncate();
        App\Survey::truncate();
        \Illuminate\Support\Facades\DB::table('surveys')->insert([
            'id' => 1,
            'title' => 'riedl',
            'slug' => 'riedl',
            'description' => 'riedl',
            'fees' => 5
        ]);

        \Illuminate\Support\Facades\DB::table('questions')->insert([
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde es oft zu kompliziert, eine berufliche Aufgabe mit Hilfe von IKT auszuführen.',
                'category'      =>  'complexity',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich benötige oft mehr Zeit als erwartet, um mit IKT in meinem beruflichen Umfeld eine Aufgabe auszuführen.',
                'category'      =>  'complexity',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich empfinde die in meinem beruflichen Umfeld zur Verfügung stehenden IKT als zu unübersichtlich.',
                'category'      =>  'complexity',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde oft nicht genug Zeit, mit neuen Funktionen der IKT im Berufsumfeld Schritt zu halten.',
                'category'      =>  'complexity',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Es würde für mich zu viel Zeit in Anspruch nehmen, komplett zu verstehen, wie ich die IKT im beruflichen Umfeld nutzen kann.',
                'category'      =>  'complexity',
            ],

            //******************************
            //invasion
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass mein Privatleben darunter leidet, dass mich durch IKT berufliche Probleme überall erreichen können.',
                'category'      =>  'invasion',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Wegen IKT ist es zu schwer für mich, mein Privatleben und mein Arbeitsleben zu trennen.',
                'category'      =>  'invasion',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Wegen IKT ist es schwieriger, klare Grenzen zwischen Privatleben und Arbeitsleben zu ziehen.',
                'category'      =>  'invasion',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Meine Work-Life Balance leidet durch die Verwendung von IKT.',
                'category'      =>  'invasion',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Die Allgegenwärtigkeit von IKT stört meine Work-Life Balance.',
                'category'      =>  'invasion',
            ],

            //******************************
            //uncertainty
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass durch IKT meine berufliche Position bedroht ist.',
                'category'      =>  'uncertainty',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich befürchte, dass ich im Beruf aufgrund der Standardisierung von Arbeitsabläufen, die durch IKT ermöglicht wird, ersetzt werden könnte.',
                'category'      =>  'uncertainty',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich kann aufgrund der Gefahr der Automatisierung durch IKT nicht optimistisch über meine langfristige berufliche Sicherheit sein.',
                'category'      =>  'uncertainty',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fürchte, dass ich von Maschinen ersetzt werden könnte.',
                'category'      =>  'uncertainty',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fürchte, dass die Digitalisierung mich meinen Job kosten wird.',
                'category'      =>  'uncertainty',
            ],

            //******************************
            //privacy
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich befürchte, dass meine Nutzung von IKT weniger vertraulich ist, als ich das gerne hätte.',
                'category'      =>  'privacy',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich befürchte, dass die Informationen, die ich via IKT austausche, nicht ausreichend gut geschützt sind.',
                'category'      =>  'privacy',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fürchte, dass böswillige Individuen (z.B. Hacker) durch IKT leicht meine Identität kopieren können.',
                'category'      =>  'privacy',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Durch IKT sind meine persönlichen Informationen zu leicht zugänglich.',
                'category'      =>  'privacy',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fürchte, dass meine persönlichen Daten im Internet leicht von anderen gestohlen werden können.',
                'category'      =>  'privacy',
            ],

            //******************************
            //overload
            [
                'survey_id'          =>  1,
                'question'         =>  'Wegen IKT habe ich zu viel zu tun.',
                'category'      =>  'overload',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Aufgrund von IKT habe ich zu viele unterschiedliche Dinge zu tun.',
                'category'      =>  'overload',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'IKT machen es für andere Personen zu einfach, mir zusätzliche Arbeit zu übermitteln.',
                'category'      =>  'overload',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich habe nie freie Zeit, weil mein Zeitplan durch IKT zu eng organisiert ist.',
                'category'      =>  'overload',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Durch IKT gibt es einen ständigen Schwall von arbeitsbezogenen Informationen, mit denen ich einfach nicht mithalten kann.',
                'category'      =>  'overload',
            ],

            //******************************
            //safety
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich muss mir zu oft Sorgen machen, ob ich schadhafte Programme herunterlade.',
                'category'      =>  'safety',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich muss mir zu oft Sorgen machen, ob ich schadhafte E-Mails erhalte.',
                'category'      =>  'safety',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fürchte, dass Hacker durch einen Fehler von mir möglicherweise Zugang zu Unternehmensgeheimnissen bekommen.',
                'category'      =>  'safety',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fühle mich ängstlich, wenn ich eine E-Mail von jemandem erhalte, den ich nicht kenne, weil es eine böswillige Attacke sein könnte.',
                'category'      =>  'safety',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'E-Mails, deren Absender ich nicht kenne, machen mich nervös.',
                'category'      =>  'safety',
            ],

            //******************************
            //social
            [
                'survey_id'          =>  1,
                'question'         =>  'Durch IKT habe ich zu viel mit den Problemen anderer zu tun.',
                'category'      =>  'social',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass durch IKT eine zu hohe Erwartung erzeugt wird, dass ich jederzeit und überall erreichbar sein soll.',
                'category'      =>  'social',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Wegen irrelevanter Kommunikation mit anderen Menschen auf sozialen Medien geht bei der Arbeit zu viel Zeit verloren.',
                'category'      =>  'social',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich habe das Gefühl, dass IKT ungewollte soziale Normen bewirken (z.B. die Erwartung, dass E-Mails direkt beantwortet werden).',
                'category'      =>  'social',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Wegen der kommunikativen Möglichkeiten der IKT (z.B. Social Media), ist es während der Arbeit zu schwer, sich eine Pause von sozialen Interaktionen zu nehmen.',
                'category'      =>  'social',
            ],

            //******************************
            //usefulness
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass sich die Anforderungen meiner Arbeit und die Funktionen der zur Verfügung stehenden IKT nicht gut genug decken.',
                'category'      =>  'usefulness',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass ich zu wenig Nutzen aus der Verwendung der bestehenden IKT für meine Arbeit generieren kann.',
                'category'      =>  'usefulness',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Die IKT, die ich bei der Arbeit nutze, sind voll von zu vielen Funktionen, die ich nie brauche.',
                'category'      =>  'usefulness',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Es braucht zu viele verschiedene Systeme, um die Aufgaben, die ich während eines normalen Arbeitstages bearbeiten muss, zu erledigen.',
                'category'      =>  'usefulness',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich denke, dass die meisten IKT, die mir bei der Arbeit zur Verfügung gestellt werden, nicht nützlich genug sind und ich meine Arbeit auch ohne sie machen könnte.',
                'category'      =>  'usefulness',
            ],

            //******************************
            //support
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich muss mir Sorgen über IT-Probleme machen, weil keine ausreichende Unterstützung für deren Behebung im Unternehmen zur Verfügung steht.',
                'category'      =>  'support',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass es zu oft vorkommt, dass bei technischen Problemen keine ausreichende Unterstützung zur Verfügung steht.',
                'category'      =>  'support',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass es zu oft vorkommt, dass technische Unterstützung nicht dann zur Verfügung steht, wenn ich sie benötige.',
                'category'      =>  'support',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich muss oft lange warten, weil technische Probleme in unserer Organisation nicht adäquat gelöst werden können.',
                'category'      =>  'support',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich fürchte, dass ein technisches Problem, das ich bei der Arbeit habe, von keinem anderen Mitarbeiter gelöst werden könnte.',
                'category'      =>  'support',
            ],

            //******************************
            //unreliability
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass ich zu oft mit unerwartetem Verhalten der verwendeten IKT konfrontiert bin (z. B. Abstürze oder lange Ladezeiten).',
                'category'      =>  'unreliability',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass ich zu viel Zeit durch technische Störungen verliere.',
                'category'      =>  'unreliability',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Ich finde, dass ich zu viel Zeit mit der Behebung von technischen Störungen verbringe.',
                'category'      =>  'unreliability',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Zu viel meiner Arbeitszeit wird dadurch verschwendet, dass ich mit der Unzuverlässigkeit der IKT zu kämpfen habe.',
                'category'      =>  'unreliability',
            ],
            [
                'survey_id'          =>  1,
                'question'         =>  'Der tägliche Ärger mit IKT (z.B. langsame Programme oder unerwartetes Systemverhalten) macht mir wirklich zu schaffen.',
                'category'      =>  'unreliability',
            ],

        ]);
    }
}
