<?php

use Illuminate\Database\Seeder;

class StaticQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\StatisQuestions::truncate();
        \Illuminate\Support\Facades\DB::table('statis_questions')->insert([
            [

                'question'          =>  'Geschlecht',
                'option_a'         =>  'Männlich',
                'option_b'      =>  'Weiblich',
                'option_c'      =>  null,
                'option_d'      =>null,
                'option_e'      =>null,
                'option_f'      =>null,

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ],

            [

                'question'          =>  'Alter',
                'option_a'         =>  'Will ich nicht sagen',
                'option_b'         =>  null,
                'option_c'         =>  null,
                'option_d'         =>  null,
                'option_e'         =>  null,
                'option_f'         =>  null,

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ],

            [

                'question'          =>  'Land',
                'option_a'         =>  'Deutschland',
                'option_b'         =>  'Deutschland',
                'option_c'         =>  'Österreich',
                'option_d'         =>  'chweiz or input field',
                'option_e'         =>  null,
                'option_f'         =>  null,

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ],
            [

                'question'          =>  'Höchster Bildungsabschluss',
                'option_a'         =>  'Kein Abschluss',
                'option_b'         =>  'Lehre / Berufsausbildung',
                'option_c'         =>  ' Fachhochschul- oder Hochschulreife (= Abitur, Matura)',
                'option_d'         =>  ' Fachhochschul- oder Hochschulabschluss',
                'option_e'         =>  ' Will ich nicht sagen',
                'option_f'         =>  null,

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ],
            [

                'question'          =>  'Derzeitige Beschäftigung',
                'option_a'         =>  'Angestellter (auch Teilzeit)',
                'option_b'         =>  'Arbeiter (auch Teilzeit)',
                'option_c'         =>  'Selbständiger',
                'option_d'         =>  'Studierender',
                'option_e'         =>  'Ohne Beschäftigung',

                'option_f'         =>  ' Will ich nicht sagen',

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ],

        ]);
    }
}
