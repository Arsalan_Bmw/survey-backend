<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::truncate();
        \Illuminate\Support\Facades\DB::table('users')->insert([
            [

                'name'          =>  'mobileuser',
                'email'         =>  'admin@gmail.com',
                'password'      =>  bcrypt('password'),
                'type'     =>  "admin",
                'status'        =>'activated',

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ],
            [

                'name'          =>  'user',
                'email'         =>  'user@gmail.com',
                'password'      =>  bcrypt('password'),
                'type'     =>  "user",
                'status'        =>'activated',

                'created_at'    =>  \Carbon\Carbon::now(),
                'updated_at'    =>  \Carbon\Carbon::now(),

            ]
        ]);
    }
}
