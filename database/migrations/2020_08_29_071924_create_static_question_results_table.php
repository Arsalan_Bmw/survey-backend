<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStaticQuestionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('static_question_results', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('survey_id')->nullable();
            $table->string('answer_one')->nullable();
            $table->string('answer_two')->nullable();
            $table->string('answer_three')->nullable();
            $table->string('answer_four')->nullable();
            $table->string('answer_five')->nullable();
            $table->enum('attempted',[0,1])->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('static_question_results');
    }
}
