<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ExtendSurveyAttemptResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('survey_attempts', function (Blueprint $table) {
            $table->string('gender')->nullable();
            $table->string('age')->nullable();
            $table->string('country')->nullable();
            $table->string('education')->nullable();
            $table->string('occupation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_attempts', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('age');
            $table->dropColumn('country');
            $table->dropColumn('education');
            $table->dropColumn('occupation');
        });
    }
}
